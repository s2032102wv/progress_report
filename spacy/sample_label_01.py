import spacy


def detect_sentence_pattern(text):
    nlp = spacy.load("en_core_web_sm")
    doc = nlp(text)
    pattern = ""

    for ent in doc.ents:
        if ent.label_ == "PERSON":
            pattern = "SVO"
        elif ent.label_ == "ORG":
            pattern = "SVOpass"
        elif ent.label_ == "DATE":
            pattern = "SVOC"
        elif ent.label_ == "GPE":
            pattern = "SVOCattr"
        elif ent.label_ == "PRODUCT":
            pattern = "SVOO"

        print(f"Entity: {ent.text}, Pattern: {pattern}")


text = "Your English sentence goes here."
detect_sentence_pattern(text)
