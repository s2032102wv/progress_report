import spacy

nlp = spacy.load("en_core_web_sm")
sent = "They consider him unfit for the job."
nlp_doc = nlp(sent)

# 代名詞
subject = [tok for tok in nlp_doc if (tok.pos_ == "PRON")]
print(subject)
# 動詞
subject = [tok for tok in nlp_doc if (tok.pos_ == "VERB")]
print(subject)
# 名詞
subject = [tok for tok in nlp_doc if (tok.pos_ == "NOUN")]
print(subject)
# 形容詞
subject = [tok for tok in nlp_doc if (tok.pos_ == "ADJ")]
print(subject)
# 設置詞
subject = [tok for tok in nlp_doc if (tok.pos_ == "ADP")]
print(subject)
# 副詞
subject = [tok for tok in nlp_doc if (tok.pos_ == "ADV")]
print(subject)

# https://yu-nix.com/archives/spacy-pos-list/
# pos_ 一覧
