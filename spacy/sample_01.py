import spacy
from spacy import displacy

nlp = spacy.load("en_core_web_sm")
doc = nlp("Apple is looking at buying U.K. startup for $1 billion.")
for token in doc:
    print(token.text, token.pos_, token.dep_)

"""for token in doc:
    print(token.text)"""

"""for token in doc:
    print(
        token.text,
        token.lemma_,  # 基本形
        token.pos_,
        token.tag_,
        token.dep_,
        token.shape_,
        token.is_alpha,
        token.is_stop,
    )

for ent in doc.ents:
    print(ent.text, ent.start_char, ent.end_char, ent.label_)"""

displacy.serve(doc)
