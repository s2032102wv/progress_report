from googletrans import Translator


def translate_text(text, dest="ja"):
    translator = Translator()
    result = translator.translate(text, dest=dest)
    return result.text


def translate_words(words, dest="ja"):
    translator = Translator()
    translated_words = []
    for i, word in enumerate(words):
        if i == 0:
            translated_word = translator.translate(word, dest=dest)
            translated_word = translated_word.text.capitalize()
        elif word in [
            "of",
            "in",
            "to",
            "for",
            "with",
            "on",
            "at",
            "from",
            "by",
            "about",
            "the",
            "it",
        ]:
            translated_word = word
        else:
            prev_word = words[i - 1]
            if prev_word[-1] in [".", ",", ":", ";"]:
                prev_word = prev_word[:-1]
            translated_word = translator.translate(prev_word + " " + word, dest=dest)
            translated_word = translated_word.text.split()[-1]
        translated_words.append(translated_word)
    return translated_words


# text = "This is a sample sentence for translation."
text = input("Enter a sentence : ")
words = text.split()
translated_words = translate_words(words)

print("Original Text:", text)
print("Translated Text:", " ".join(translated_words))
