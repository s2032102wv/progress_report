import nltk
from nltk.tag import pos_tag
from nltk.tokenize import word_tokenize


def get_5patterns(text):
    # Tokenize the text into words
    words = word_tokenize(text)

    # Tag the words with their part of speech
    tagged_words = pos_tag(words)

    # Extract the subject, verb, object, complement, and adverb
    subject = ""
    verb = ""
    obj = ""
    complement = ""
    adverb = ""

    for i in range(len(tagged_words)):
        word = tagged_words[i][0]
        tag = tagged_words[i][1]

        if "subj" in tag.lower():
            subject = word
        elif "vb" in tag.lower():
            verb = word
        elif "obj" in tag.lower():
            obj = word
        elif "comp" in tag.lower():
            complement = word
        elif "adv" in tag.lower():
            adverb = word

    # Return the 5 patterns as a dictionary
    return {
        "SVO": f"{subject} {verb} {obj}",
        "SVC": f"{subject} {complement}",
        "SVOO": f"{subject} {verb} {obj} {obj}",
        "SVOC": f"{subject} {verb} {obj} {complement}",
        "SVOA": f"{subject} {verb} {obj} {adverb}",
    }


# Example usage
text = input("Enter an English sentence: ")
patterns = get_5patterns(text)
for pattern, sentence in patterns.items():
    print(f"{pattern}: {sentence}")
