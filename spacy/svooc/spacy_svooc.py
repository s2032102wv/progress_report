import spacy


def get_5patterns(text):
    # Load the English language model
    nlp = spacy.load("en_core_web_sm")

    # Parse the text with spaCy
    doc = nlp(text)

    # Extract the subject, verb, object, complement, and adverb
    subject = ""
    verb = ""
    obj = ""
    complement = ""
    adverb = ""

    for token in doc:
        if "subj" in token.dep_:
            subject = token.text
        elif "obj" in token.dep_:
            obj = token.text
        elif "comp" in token.dep_:
            complement = token.text
        elif "advmod" in token.dep_:
            adverb = token.text
        elif "ROOT" in token.dep_:
            verb = token.text

    # Return the 5 patterns as a dictionary
    return {
        "SVO": f"{subject} {verb} {obj}",
        "SVC": f"{subject} {complement}",
        "SVOO": f"{subject} {verb} {obj} {obj}",
        "SVOC": f"{subject} {verb} {obj} {complement}",
        "SVOA": f"{subject} {verb} {obj} {adverb}",
    }


# Example usage
text = input("Enter an English sentence: ")
patterns = get_5patterns(text)
for pattern, sentence in patterns.items():
    print(f"{pattern}: {sentence}")
