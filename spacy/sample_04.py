import nltk


def get_sentence_type(sentence):
    tokens = nltk.word_tokenize(sentence)
    tagged = nltk.pos_tag(tokens)
    grammar = r"""
        SVOO: {<NN.*><VB.*><DT>?<JJ.*>*<NN.*><IN><NN.*>}
        SVOC: {<NN.*><VB.*><JJ.*>*<IN><NN.*>}
        SVO: {<NN.*><VB.*><DT>?<JJ.*>*<NN.*>}
        SV: {<NN.*><VB.*>}
    """
    cp = nltk.RegexpParser(grammar)
    result = cp.parse(tagged)
    for subtree in result.subtrees():
        if subtree.label() == "SVOO":
            return "SVOO"
        elif subtree.label() == "SVOC":
            return "SVOC"
        elif subtree.label() == "SVO":
            return "SVO"
        elif subtree.label() == "SV":
            return "SV"
    return "Other"


sentence = input("Enter a sentence: ")
print(get_sentence_type(sentence))


# 全然だめ。殆どの英文ではOtherと出力される。
