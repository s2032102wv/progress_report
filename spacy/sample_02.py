import spacy
from spacy import displacy

nlp = spacy.load("en_core_web_sm")
doc = nlp("Apple is looking at buying U.K. startup for $1 billion.")

"""for token in doc:
    print(token.text, list(token.children))#トークンを持つ子供を山荘
    """


def show_tree(token, dep=0):
    print(dep * "_" + token.text)
    for child in token.children:
        show_tree(child, dep + 1)


for sent in doc.sents:
    show_tree(sent.root)

# displacy.serve(doc)


# 上記の参考ページ「https://yu-nix.com/archives/spacy-print-dep-tree/」
