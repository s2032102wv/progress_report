import spacy
from nltk import Tree

en_nlp = spacy.load("en_core_web_sm")

doc = en_nlp("The quick brown fox jumpus over the lazy dog.")


def to_nltk_tree(node):
    if node.n_lefts + node.n_rights > 0:
        return Tree(node.orth_, [to_nltk_tree(child) for child in node.children])
    else:
        return node.orth_


def tok_format(tok):
    return "_".join([tok.orth_, tok.tag_])


def to_nltk_tree(node):
    if node.n_lefts + node.n_rights > 0:
        return Tree(tok_format(node), [to_nltk_tree(child) for child in node.children])
    else:
        return tok_format(node)


[to_nltk_tree(sent.root).pretty_print() for sent in doc.sents]
