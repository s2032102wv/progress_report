import spacy
from spacy import displacy

# 樹形図
nlp = spacy.load("en_core_web_sm")
word = input("Enter a sentence:")
doc = nlp(word)
for token in doc:
    print(f"{token.text=}", "\t" f"{token.pos_=}", "\t" f"{token.dep_=}")

# displacy.serve(doc, style="dep", page=True, port=5000, host="127.0.0.1")

# depタグについて、これを利用して、5文型を出力しようとすることを考えたが、Theyとhimのdepタグはどちらもnsubj「主語（名詞性）」になっており、判別が困難では？
# 文に2つ以上PRON(nsubj)があった場合、後ろのPRON(nsubj)を主語ではなく、目的語とかに変更するようにすれば良いかな？
