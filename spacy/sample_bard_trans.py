"""from googletrans import Translator

translator = Translator()
sentence = input("Enter a sentence:")
words = sentence.split()

for word in words:
#tryに関しては、教授との進捗報告で付け加えられたもの
    try:
        result = translator.translate(word, dest="ja")
    except TypeError:
        print(f"{word} -> {word}")
        pass
    else:
        print(f"{word} -> {result.text}")"""

from googletrans import Translator


def translate_word_by_word(text, to_language):
    words = text.split()
    translated_words = []
    translator = Translator(service_urls=["translate.google.com"])
    for word in words:
        if word.lower() in [
            "in",
            "on",
            "at",
            "to",
            "from",
            "with",
            "by",
            "for",
            "the",
        ]:
            translated_word = word
        else:
            translated_word = translator.translate(word, dest=to_language).text
        translated_words.append(translated_word)
    return " ".join(translated_words)


text = input("Enter a sentence: ")
to_language = "ja"
translated_text = translate_word_by_word(text, to_language)
print(translated_text)
