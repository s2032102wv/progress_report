import spacy
from spacy import displacy

# 言語モデルを読み込む
nlp = spacy.load("en_core_web_sm")


# 解析したい英文を処理する
text = "They consider him unfit for the job."
doc = nlp(text)


def get_japanese_pos(pos):
    # 品詞を英語から日本語に変換する辞書
    pos_dict = {
        "ADJ": "形容詞",
        "ADP": "助詞",
        "ADV": "副詞",
        "AUX": "助動詞",
        "CONJ": "接続詞",
        "CCONJ": "等位接続詞",
        "DET": "限定詞",
        "INTJ": "間投詞",
        "NOUN": "名詞",
        "NUM": "数詞",
        "PART": "助動詞",
        "PRON": "代名詞",
        "PROPN": "固有名詞",
        "PUNCT": "句読点",
        "SCONJ": "従属接続詞",
        "SYM": "記号",
        "VERB": "動詞",
        "X": "その他",
        "SPACE": "空白",
    }
    return pos_dict.get(pos, "不明")


def get_japanese_dep(dep):
    # 依存関係を英語から日本語に変換する辞書
    dep_dict = {
        "ROOT": "ルート",
        "acl": "連体修飾節",
        "acomp": "形容詞補語",
        "advcl": "副詞節",
        "advmod": "副詞修飾語",
        "amod": "形容詞修飾語",
        "appos": "同格",
        "attr": "属性",
        "aux": "助動詞",
        "auxpass": "受動態の助動詞",
        "case": "格表示",
        "cc": "等位接続詞",
        "ccomp": "従属節の補語",
        "clf": "分類詞",
        "compound": "複合語",
        "conj": "接続詞句",
        "cop": "連結詞",
        "csubj": "主部部分の従属節",
        "csubjpass": "受動態の主部部分の従属節",
        "dative": "与格",
        "dep": "不明",
        "det": "限定詞",
        "dobj": "直接目的語",
        "expl": "転成成分",
        "intj": "間投詞",
        "mark": "節の標識",
        "meta": "メタ要素",
        "neg": "否定詞",
        "nmod": "名詞修飾節",
        "npmod": "名詞修飾節",
        "nsubj": "主部部分",
        "nsubjpass": "受動態の主部部分",
        "nummod": "数詞修飾節",
        "oprd": "補語",
        "parataxis": "並立句",
        "pcomp": "前置補語",
        "pobj": "前置目的語",
        "poss": "所有格",
        "preconj": "接頭辞",
        "predet": "冠詞",
        "prep": "前置詞",
        "prt": "助詞",
        "punct": "句読点",
        "quantmod": "数量修飾語",
        "relcl": "関係節",
        "root": "ルート",
        "vocative": "呼格",
        "xcomp": "連結節",
    }
    return dep_dict.get(dep, "不明")


# 結果を出力する
print(f"原文: {text}")


for token in doc:
    print(
        f"単語: {token.text}, 品詞: {token.pos_}({get_japanese_pos(token.pos_)}), 依存関係: {token.dep_}({get_japanese_dep(token.dep_)})"
    )

displacy.serve(
    doc,
    options={
        "conpact": "True",
        "color": "#ffffff",
        "bg": "#0099ff",
        "font": "sans-serif",
    },
)
