import spacy


def detect_sentence_pattern(text):
    nlp = spacy.load("en_core_web_sm")
    doc = nlp(text)
    pattern = ""

    for sent in doc.sents:
        if sent.root.dep_ == "nsubj":
            pattern = "SVO"
        elif sent.root.dep_ == "nsubjpass":
            pattern = "SVOpass"
        elif sent.root.dep_ == "dobj":
            pattern = "SVOC"
        elif sent.root.dep_ == "attr":
            pattern = "SVOCattr"
        elif sent.root.dep_ == "pobj":
            pattern = "SVOO"

        print(f"Sentence: {sent.text}, Pattern: {pattern}")


text = input("Enter a sentence : ")
detect_sentence_pattern(text)
