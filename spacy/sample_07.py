import spacy


def extract_subject_verb_object(sentence):
    nlp = spacy.load("en_core_web_sm")
    doc = nlp(sentence)
    subject = None
    verb = None
    object = None
    indirect_object = None
    prepositional_object = None
    for sent in doc.sents:
        for token in sent:
            if "nsubj" in token.dep_:
                subject = token.text
            if "dobj" in token.dep_:
                object = token.text
            if "iobj" in token.dep_:
                indirect_object = token.text
            if "pobj" in token.dep_:
                prepositional_object = token.text
            if "ROOT" in token.dep_:
                verb = token.text

    return subject, verb, object, indirect_object, prepositional_object


sentence = "They consider him unfit for the job."
(
    subject,
    verb,
    object,
    indirect_object,
    prepositional_object,
) = extract_subject_verb_object(sentence)

print(f"Subject: {subject}")
print(f"Verb: {verb}")
print(f"Object: {object}")
