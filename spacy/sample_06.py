import spacy

nlp = spacy.load("en_core_web_sm")


def get_subject_verb_object(sentence):
    doc = nlp(sentence)
    subject = ""
    verb = ""
    # object_ = ""
    object_ = []
    complement = ""

    count_subj = 0

    for token in doc:
        if "subj" in token.dep_:
            # subjが複数ある英文だと1番目のsubjが2番目のsubjに上書きされてしまうことから、himが主語として出力されていた。なので、一番最初のsubjを上書きされないようにした。このプログラムだと、一番最初のsubjが主語にならない英文には無意味とかす。
            if count_subj == 0:
                subject = token.text
                count_subj += 1
            else:
                object_.append(token.text)

        elif "obj" in token.dep_:
            object_.append(token.text)
        elif "ROOT" in token.dep_:
            verb = token.text

    for possible_complement in doc:
        if (
            possible_complement.head.text == verb
            and possible_complement.dep_ == "acomp"
        ):
            complement = possible_complement.text

    return subject.strip(), verb.strip(), object_, complement.strip()


sentence = input("Enter a sentence : ")
subject, verb, object_, complement = get_subject_verb_object(sentence)

print(f"Subject: {subject}")
print(f"Verb: {verb}")
print(f"Object: {object_}")
print(f"Complement: {complement}")

if object_ == "":
    if complement == "":
        print("SV")
    else:
        print("SVC")
else:
    if complement == "":
        if len(object_) < 2:
            print("SVO")
        else:
            print("SVOO")
    else:
        print("SVOC")


# himとかのdepタグがsubjだから、主語の方の判定になってしまい、objに行かない。(修正した)
# 名詞が合ったら無条件でobjにしている。これは、前置詞＋名詞を判定していないことから起きていると予想。
# me はdative(与格)らしく、objからはじかれる meがsubjとして出力される場合もある。それに加えて、名詞の単語がccompと出力される場合もあり、名詞がなるdepタグを網羅しないと正しく表示されなそう。
