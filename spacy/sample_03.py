import spacy

nlp = spacy.load("en_core_web_sm")


def get_sentence_type(sentence):
    doc = nlp(sentence)
    for token in doc:
        if token.dep_ == "ROOT":
            if token.children:
                obj_flag = False
                for child in token.children:
                    if child.dep_ == "dobj":
                        obj_flag = True
                if obj_flag:
                    return "SVOO"
                else:
                    return "SVO"
            else:
                return "SV"
        elif token.dep_ == "xcomp":
            return "SVOC"
    return "Other"


sentence = input("Enter a sentence: ")
print(get_sentence_type(sentence))


# このシステムは多くの場合、SVOと表示する。特に、補語に関して表示されない。実際に、SVOCの英文を複数入力したが、多くの文でSVOと出力がされた。
