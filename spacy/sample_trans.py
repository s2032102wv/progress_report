"""from googletrans import Translator

translator = Translator()
text_en = "Apple is looking at buying U.K. startup for $1 billion."
text_ja = translator.translate(text_en, src="en", dest="ja").text

print(text_ja)

# 結果⇨Factory work programmer
"""

"""from googletrans import Translator


def translate_text(text, dest="ja"):
    translator = Translator()
    result = translator.translate(text, dest=dest)
    return result.text


def translate_words(words, dest="ja"):
    translator = Translator()
    translated_words = []
    for word in words:
        translated_word = translator.translate(word, dest=dest)
        translated_words.append(translated_word.text)
    return translated_words


text = input("Enter a sentence  : ")
result = translate_text(text)
print(result)

words = input("Enter a sentence  : ")
result = translate_words(words)
print(result)"""

from googletrans import Translator


def translate_text(text, dest="ja"):
    translator = Translator()
    result = translator.translate(text, dest=dest)
    return result.text


"""def translate_words(words, dest="ja"):
    translator = Translator()
    translated_words = []
    for word in words:
        translated_word = translator.translate(word, dest=dest)
        translated_words.append(translated_word.text)
    return translated_words
"""


def translate_words(words, dest="ja"):
    translator = Translator()
    translated_words = []
    for word in words:
        if word in [
            "of",
            "in",
            "to",
            "for",
            "with",
            "on",
            "at",
            "from",
            "by",
            "about",
            "the",
            "it",
            # 先頭大文字
            "Of",
            "In",
            "To",
            "For",
            "With",
            "On",
            "At",
            "From",
            "By",
            "About",
            "The",
            "It",
            """# 助動詞
            "can",
            "cannot",
            "can't",
            "could",
            "will",
            "would",
            "do",
            "does",
            "did",
            "may",
            "might",
            "must",
            "shall",
            "should",
            # 大文字
            "Can",
            "Cannot",
            "Can't",
            "Could",
            "Will",
            "Would",
            "Do",
            "Does",
            "Did",
            "May",
            "Might",
            "Must",
            "Shall",
            "Should",""",
        ]:
            translated_word = word
        else:
            translated_word = translator.translate(word, dest=dest)
            translated_word = translated_word.text
        translated_words.append(translated_word)
    return translated_words


text = input("Enter a text : ")
words = text.split()
translated_words = translate_words(words)

print("Original Text:", text)
print("Translated Text:", " ".join(translated_words))
