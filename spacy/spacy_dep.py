import spacy


def print_dep_tags(text):
    nlp = spacy.load("en_core_web_sm")
    doc = nlp(text)
    for token in doc:
        print(token.text, token.dep_)


text = input("Enter in words : ")
print_dep_tags(text)
